<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <title>FourthExercise</title>
    <link rel="stylesheet" href="style.css">
    <style>
        /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
        .error {
            border: 2px solid red;

        }
    </style>
</head>

<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    // Выводим все сообщения.
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<div class="nameForm m-auto">Форма</div>
<div class="container border rounded-5 shadow">

    <form action="" method="POST" >
        <div class="flex-column">
            <div class="c">
                ФИО:

                <input name="fio" placeholder="Фамилия Имя Отчество" <?php if ($errors['fio']) {
                    print 'class="error"';
                } ?> value="<?php print $values['fio']; ?>" />

            </div>
            <div class="c">
                E-mail:

                <input type="email" name="email" placeholder="Введите e-mail" <?php if ($errors['email']) {
                    print 'class="error"';
                } ?> value="<?php print $values['email']; ?>">

            </div>
            <div class="c">
                <p>Ваш год рождения:</p>
                <input name="year" type="date" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year_value']; ?>" id="dr">
            </div>
            <div class="c">
                <p>Пол:</p>
                <label>
                    <input type="radio" name="radio-pol" checked="checked" value="M"<?php if($values['radio-pol'] == 'M') {print 'checked';}?> <?php if ($errors['radio-pol']) {print 'class="error"';} ?>/>М
                </label>
                <label>
                    <input type="radio" name="radio-pol" value="W" <?php if($values['radio-pol'] == 'W') {print 'checked';}?> <?php if ($errors['radio-pol']) {print 'class="error"';} ?>/>Ж
                </label>

                <p>Количество конечностей</p>
                <label>
                    <input type="radio" name="radio-kon" value="0" <?php if($values['radio-kon'] == 0) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />0
                </label>
                <label>
                    <input type="radio" name="radio-kon" value="1"<?php if($values['radio-kon'] == 1) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?> />1
                </label>
                <label>
                    <input type="radio" name="radio-kon" value="2" <?php if($values['radio-kon'] == 2) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>2
                </label>
                <label>
                    <input type="radio" name="radio-kon" value="3" <?php if($values['radio-kon'] == 3) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>3
                </label>
                <label>
                    <input type="radio" checked="checked" name="radio-kon" value="4" <?php if($values['radio-kon'] == 4) {print 'checked';}?> <?php if ($errors['radio-kon']) {print 'class="error"';} ?>/>4
                </label>
            </div>
            <div class="c">

                <p><strong>Сверхспособности</strong></p>

                <label>
                    <select name="sp-sp[]" multiple=multiple>
                        <option value="1" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
                            <?php
                            $arr = str_split($values['sp-sp']);
                            foreach($arr as $el)
                                if ($el == 1)
                                    print 'selected';
                            ?>
                        >Суперсила</option>
                        <option value="2" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
                            <?php
                            $arr = str_split($values['sp-sp']);
                            foreach($arr as $el)
                                if ($el == 2)
                                    print 'selected';
                            ?>
                        >Суперскорость</option>
                        <option value="3" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
                            <?php
                            $arr = str_split($values['sp-sp']);
                            foreach($arr as $el)
                                if ($el == "3")
                                    print 'selected';
                            ?>
                        >Интеллект</option>
                        <option value="4" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
                            <?php
                            $arr = str_split($values['sp-sp']);
                            foreach($arr as $el)
                                if ($el == "4")
                                    print 'selected';
                            ?>
                        >Невидимость</option>
                        <option value="5" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
                            <?php
                            $arr = str_split($values['sp-sp']);
                            foreach($arr as $el)
                                if ($el == "5")
                                    print 'selected';
                            ?>
                        >Суперзрение</option>
                        <option value="6" <?php if ($errors['sp-sp']) {print 'class="error"';} ?>
                            <?php
                            $arr = str_split($values['sp-sp']);
                            foreach($arr as $el)
                                if ($el == "6")
                                    print 'selected';
                            ?>
                        >Телекинез</option>
                    </select>
                </label>

            </div>
            <div class="c">
                <p id="bio">Биография:</p>

                <textarea placeholder="Расскажите о себе" name="biography" rows="3" cols="40" <?php if ($errors['biography']) {
                    print 'class="error"';
                } ?> ><?php print $values['biography']; ?></textarea>
            </div>

            <div class="c">
                <label>
                    С контрактом ознакомлен(а)
                    <input type="checkbox" name="check-ok" checked="checked">
                </label>
            </div>
        </div>
        <div class="m-auto button p-3">
            <input type="submit" value="Отправить" class="btn btn-success ">
        </div>
    </form>
</div>
</body>

